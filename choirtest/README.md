# Choir test

This test has a set of six predefined scenes using a few test videos available in my website and takes in messages 
from TouchOSC and swaps between the scenes. It is ameant as a kick test of the Playa video player.

This test can receive OSC messages on port 54321 (udp).

## Manual method

To run it, simply do:

```
cd app
npm install
```

and then

```
npm start
```

### Running as a docker container

Two scripts are provided, one that builds the image and one that runs a container with that image:

Running `./makeimage.sh` will create the `choirtest:latest` image and to run a container using that image use `./runcontainer.sh`

The ports exposed and mapped in the container need to be UDP.

