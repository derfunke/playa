const osc = require("osc")
const mqtt = require("mqtt")

// ////////////////////////////////////////////////////////////////
// scenes
// ////////////////////////////////////////////////////////////////
var videos = [
    {
      url: "http://karajan.local/videos/choirtest/creature1.mp4",
      type: "video/mp4",
      title: "one"
    },
    {
      url: "http://karajan.local/videos/choirtest/creature2.mp4",
      type: "video/mp4",
      title: "Tunnel"
    },
    {
      url: "http://karajan.local/videos/choirtest/creature3.mp4",
      type: "video/mp4",
      title: "Dancer"
    },
    {
      url: "http://karajan.local/videos/choirtest/creature4.mp4",
      type: "video/mp4",
      title: "Train"
    },
    {
      url: "http://karajan.local/videos/choirtest/creature5.mp4",
      type: "video/mp4",
      title: "Dancer palm"
    },
    {
      url: "http://karajan.local/videos/choirtest/creature6.mp4",
      type: "video/mp4",
      title: "Dancer palm"
    },
    {
      url: "http://karajan.local/videos/choirtest/creature7.mp4",
      type: "video/mp4",
      title: "Wireframe"
    }
]

var scenes = [
    [
        { toId: "player1", video: videos[0] },
        { toId: "player2", video: videos[2] },
        { toId: "player3", video: videos[3] },
        { toId: "player4", video: videos[4] },
        { toId: "player5", video: videos[1] },
        { toId: "player6", video: videos[6] },
        { toId: "player7", video: videos[6] },
        { toId: "player8", video: videos[0] },
    ],
    [
        { toId: "player1", video: videos[1] },
        { toId: "player2", video: videos[4] },
        { toId: "player3", video: videos[2] },
        { toId: "player4", video: videos[0] },
        { toId: "player5", video: videos[1] },
        { toId: "player6", video: videos[0] },
        { toId: "player7", video: videos[1] },
        { toId: "player8", video: videos[0] },
    ],
    [
        { toId: "player1", video: videos[1] },
        { toId: "player2", video: videos[4] },
        { toId: "player3", video: videos[2] },
        { toId: "player4", video: videos[5] },
        { toId: "player5", video: videos[1] },
        { toId: "player6", video: videos[2] },
        { toId: "player7", video: videos[1] },
        { toId: "player8", video: videos[2] },
    ],
    [
        { toId: "player1", video: videos[3] },
        { toId: "player2", video: videos[2] },
        { toId: "player3", video: videos[4] },
        { toId: "player4", video: videos[5] },
        { toId: "player5", video: videos[3] },
        { toId: "player6", video: videos[2] },
        { toId: "player7", video: videos[3] },
        { toId: "player8", video: videos[2] },
    ],
    [
        { toId: "player1", video: videos[6] },
        { toId: "player2", video: videos[6] },
        { toId: "player3", video: videos[6] },
        { toId: "player4", video: videos[6] },
        { toId: "player5", video: videos[6] },
        { toId: "player6", video: videos[6] },
        { toId: "player7", video: videos[6] },
        { toId: "player8", video: videos[6] },
    ],
]

// ////////////////////////////////////////////////////////////////
let sceneIdx = 0;

function dispatch_players_messages() {
    scenes[sceneIdx].forEach( (item) => {
        let topic = `${item.toId}/url`
        client.publish(topic, item.video.url)
        console.debug(`>> MQTT out -> ${topic}, video ${item.video.url}`)
    })
}

function change_scene() {
    sceneIdx++
    if(sceneIdx >= scenes.length) { sceneIdx = 0 }

    dispatch_players_messages()
}

// ////////////////////////////////////////////////////////////////
// MQTT stuff
// ////////////////////////////////////////////////////////////////
var config = {
  port: 1883,
  host: 'mqtt://mosquitto',
  clientId: 'choirtest-osc-bridge'
};

console.info(`Connnecting to mqtt broker ${config.host}:${config.port}`)
let client = mqtt.connect( config.host, config )

client.on("connect", function () {
  console.info(`Connected!`)
  client.subscribe("presence", function (err) {
    if (!err) {
      client.publish("presence", config.id)
    }
  })
})


// ////////////////////////////////////////////////////////////////
// OSC stuff
// ////////////////////////////////////////////////////////////////
let port = 54321

var udpPort = new osc.UDPPort({
    localAddress: "0.0.0.0",
    localPort: port,
    metadata: true
});

console.info(`Listening on port ${port}`)

// Listen for incoming OSC messages.
udpPort.on("message", function (msg, timeTag, info) {
    console.log("<< OSC in <- ", msg);

    if( msg && msg.args.length ) {
      if( (msg.args[0].value == 0) 
          && (msg.address.includes("/2/push"))  /* address: /2/push16 */ 
      ) { 
          change_scene();
      }
    }
});

udpPort.open();

