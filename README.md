# Playa 🏖️

A browser-based synchronised multichannel video player for video art. Playa allows you to:

- play a different video in each device
- start multiple videos in different devices at the same time
- mute a specific video on a specific device

### What you need to use Playa

All you need is:
- a device with a web browser, such as your phone, a computer, a smart TV, a tablet, etc.
- your video files in a "web acccesible location", like your website
- optionally if you want to run off-the-internet, you can create an orchestration node, and for that you will need a raspberry pi.

