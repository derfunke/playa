import mqtt.*;

MQTTClient client;

void setup() {
  client = new MQTTClient(this);
  client.connect("mqtt://playatest:b2TLJGMTy4Ptr6B2@playatest.cloud.shiftr.io", "p5-score");
}

void playa_dispatch_video(String player, String url) {
  String topic = player+"/url"; /* this is because in the message map, we send a new video using the 'playerID/url' topic */
  println("Sending message on topic " + topic + " to play " + url);
  client.publish(topic, url);
}

void draw() {
  // this is basically a scene, let's say SCENE 1
  println("SCENE 1 =================================================================");
  playa_dispatch_video("playerAA", "https://cdn.derfunke.net/videos/abstract.mp4");
  playa_dispatch_video("playerAB", "https://cdn.derfunke.net/videos/tunnel.mp4");
  playa_dispatch_video("playerAC", "https://cdn.derfunke.net/videos/dancer.mp4");
  playa_dispatch_video("playerAD", "https://cdn.derfunke.net/videos/train.mp4");

  delay(10*1000);
  
  // scene 2
  println("SCENE 2 =================================================================");
  playa_dispatch_video("playerAA", "https://cdn.derfunke.net/videos/abstract.mp4");
  playa_dispatch_video("playerAB", "https://cdn.derfunke.net/videos/abstract.mp4");
  playa_dispatch_video("playerAC", "https://cdn.derfunke.net/videos/abstract.mp4");
  playa_dispatch_video("playerAD", "https://cdn.derfunke.net/videos/abstract.mp4");

  delay(10*1000);

  // scene 3
  println("SCENE 3 =================================================================");
  playa_dispatch_video("playerAA", "https://cdn.derfunke.net/videos/abstract.mp4");
  playa_dispatch_video("playerAB", "https://cdn.derfunke.net/videos/train.mp4");
  playa_dispatch_video("playerAC", "https://cdn.derfunke.net/videos/abstract.mp4");
  playa_dispatch_video("playerAD", "https://cdn.derfunke.net/videos/train.mp4");
  
  delay(10*1000);
}

void clientConnect() {
}

void connectionLost() {
}

void messageReceived(String topic, byte[] payload) {
  println("<< got message: " + topic + " < " + new String(payload) );
}
