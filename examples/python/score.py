import paho.mqtt.client as mqtt
import time

def on_message(client, userdata, message):
    print("<< message ", str(message.payload.decode("utf-8") ))

client = mqtt.Client("python-score", True, None, mqtt.MQTTv31)
client.connect("playatest.cloud.shiftr.io", 1883)
client.username_pw_set("playatest","b2TLJGMTy4Ptr6B2")
client.on_message = on_message
# we need to start the polling loop, otherwise there will be no connection made
client.loop_start()

# scene 1
print("SCENE 1")
client.publish("playerAA/url", "https://cdn.derfunke.net/videos/abstract.mp4")
client.publish("playerAB/url", "https://cdn.derfunke.net/videos/abstract.mp4")
client.publish("playerAC/url", "https://cdn.derfunke.net/videos/abstract.mp4")
client.publish("playerAD/url", "https://cdn.derfunke.net/videos/abstract.mp4")

time.sleep(10)

# scene 2
print("SCENE 2")
client.publish("playerAA/url", "https://cdn.derfunke.net/videos/tunnel.mp4")
client.publish("playerAB/url", "https://cdn.derfunke.net/videos/dancer.mp4")
client.publish("playerAC/url", "https://cdn.derfunke.net/videos/train.mp4")
client.publish("playerAD/url", "https://cdn.derfunke.net/videos/wireframe.mp4")

time.sleep(10)

# scene 3
print("SCENE 3")
client.publish("playerAA/url", "https://cdn.derfunke.net/videos/dancer.mp4")
client.publish("playerAB/url", "https://cdn.derfunke.net/videos/abstract.mp4")
client.publish("playerAC/url", "https://cdn.derfunke.net/videos/dancer.mp4")
client.publish("playerAD/url", "https://cdn.derfunke.net/videos/abstract.mp4")

time.sleep(10)

client.loop_end()