#!/bin/sh -e

LXDE_AUTOSTART_PATH=/home/pi/.config/lxsession/LXDE-pi
URL_AUTHORIZED_KEYS=http://bernie.local/config/authorized_keys.asc

# install and uninstall packages
apt-get remove -qq -y unattended-upgrades
apt-get install -qq -y patch unclutter

# get & install authorized_keys
curl -fsSL $URL_AUTHORIZED_KEYS -o /tmp/authorized_keys.asc

touch /tmp/player_configured

install -o pi -d -m 700 /home/pi/.ssh
install -o pi -m 600 /tmp/authorized_keys.asc /home/pi/.ssh/authorized_keys
chown -R pi:pi /home/pi/.ssh

# create LXDE configuration file
install -o pi -d -m 755 $LXDE_AUTOSTART_PATH
cat > $LXDE_AUTOSTART_PATH/autostart <<- EOM
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
#@xscreensaver -no-splash
#point-rpi

# disable screen blanking
@xset s noblank
@xset s off
@xset -dpms

# hide mouse pointer
@unclutter -idle 2

# start up browser window
@chromium-browser --start-fullscreen --autoplay-policy=no-user-gesture-required --start-maximized --kiosk --disable-session-crashed-bubble http://bernie.local/playa/
EOM

chown -R pi:pi /home/pi/.config
