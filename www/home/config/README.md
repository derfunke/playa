In your Raspberry Pi you need to create a new systemd service that will take care of auto-updating the player configuration on startup. 

You need to create a file `/etc/systemd/system/playa-config.service` with these contents:

```
[Unit]
Description=Playa client configuration service
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/bin/bash /usr/sbin/playa-client-configurator.sh

[Install]
WantedBy=multi-user.target
```
