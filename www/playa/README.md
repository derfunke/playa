# Playa

Playa is a video player that can be orchestrated using mqtt messages.

This is a browser-based video player designed to be used in multichannel, multidevice video installations. Orchestration works using MQTT messages, that way you can tell each device which video channel to play at any given time. Video files must be available somewhere accessible via HTTP, like a raspberry pi or some other computer that can act as a webserver, either on the internet or in a local network without internet access.

## MQTT message map

From orchestrator to Playa, these are the accepted incoming messages:

| topic              | payload                                  | description                             |
| ------------------ | ---------------------------------------- | --------------------------------------- |
| `<playerID>/url`     | http://cdn.example.com/videos/vid001.mp4 | play the video indicated by the payload |
| `<playerID>/command` | mute                                     | mute audio on player                    |
| `<playerID>/command` | noloop                                   | disable looping                         |
| `<playerID>/command` | loop                                     | enable looping                          |
| `<playerID>/command` | pause                                    | pause video                             |
| `<playerID>/command` | resume                                   | restart playback                        |
| `<playerID>/status` | n.a.                                   | query the player for its current status, is it playing, if so, what video?, etc.  |
| `<playerID>/overlay` | overlay text    | show an overlay with text on top of the currently playing video, overlay will be visible for 6 seconds |

From Playa to orchestrator these are the outgoing mqtt messages:

| topic             | payload    | description                                                        |
| ----------------- | ---------- | ------------------------------------------------------------------ |
| presence          | `<playerID>` | sent when player with id = <playerid> becomes online               |
| status          | `{ playerid: <playerID>, isplaying: "true", url: "#" }` | reply to the `<playerID>/status` message, observe that return value is a JSON string containing some values in a dictionary |
| `events/<playerID>` | ended      | video finished playing (never triggered if player is in loop mode) |

## How to use

To use Playa you need three things:

- **PLAYBACK DEVICE** can be a web browser (this can be on any device)
- **FILE SERVER** can be a web server where video files are stored
- **ORCHESTRATION SERVER** can be any mqtt broker (you can use shiftr.io)

### On the device

Open the URL of the server in your device, e.g. https://playa.derfunke.net. You will see a welcome screen where you can configure the player. You must give the player an ID, this information will be stored in the devices browser and will be remembered in fugure sessiong.

You should also configure Playa to use your mqtt broker. By default Playa uses a shiftr.io server, but you can use any mqtt broker you like, including one in your local network that is not accessible from the internet.

### Creating your own server in a Raspberry pi

Playa comes with a script that will help you setup the necessary software packages to turn a raspberry pi into a file server and an orchestration server using a single computer.

You can find the script in `raspberry/playainstall.sh`, run as root.

```
$ sudo raspberrypi/playainstall.h
```

#### Testing the player manually

You can download [MQTT Explorer](http://mqtt-explorer.com/), this tool allows you to craft MQTT messages and look at the MQTT traffic.

#### Autoplay browser policy

Read more: https://developer.chrome.com/blog/autoplay/

It can be bypassed by starting the browser like so:

`chrome.exe --autoplay-policy=no-user-gesture-required`

### Building web app

Deploy the generated dist directory.

Needs `yarn` and `nodejs 12.x`.

```
yarn install
```

Making build: `yarn run build`

#### Special build for the karajan raspberry

The raspberry keeps a bunch of web applications in different URLs and `parcel` defaults to building a web app where all resources point to root, assuming it will be in it's own (sub)domain.

The web server configuration puts the player in the `/playa` path, but all assets and resources of the playa web app point to `/`, so we need to rebuild the application for deployment in karajan. Like so:

```
npm install
yarn run buildinpath
```



