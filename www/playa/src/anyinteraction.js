import _ from 'lodash'

let ListenForAnyInteraction = function () {

  let lastId = Date.now();
  let callbacks = {};
  
  this.onInteraction = (callback) => {
    lastId++;
    callbacks[++lastId] = callback;
    return
  }

  this.handleInteraction = _.debounce(() => {
//    console.log('interaction is happening...')
    for (let i in callbacks) {
      if (typeof callbacks[i] === 'function') {
        callbacks[i]();
      } else {
        delete callbacks[i];
      }
    }
  }, 1000)

  // events that register any kind of user interaction
  // var activityEvents = [
  //     'mousedown', 'mousemove', 'keydown',
  //     'scroll', 'click', 'touchstart'
  // ];

  // activityEvents.forEach(function(event) {
  //     document.body.addEventListener(event, this.handleInteraction, true);
  // });

  document.body.addEventListener('mousemove', this.handleInteraction)
  document.body.addEventListener('scroll', this.handleInteraction)
  document.body.addEventListener('keydown', this.handleInteraction)
  document.body.addEventListener('click', this.handleInteraction)
  document.body.addEventListener('touchstart', this.handleInteraction)

}

let anyinteraction = new ListenForAnyInteraction()
//export default anyinteraction;

export { ListenForAnyInteraction }
