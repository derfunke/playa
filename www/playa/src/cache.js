class Cache {
  constructor(key, expire = -1 /* in seconds, -1 means no expiry */) {
    this.key = key
    this.expire = expire
    this.items = this.load()
    console.debug(">> Cache ctor ")
    console.log(">> config contents:")
    console.log(this.items)
  }

  save() {
    localStorage.setItem(this.key, JSON.stringify(this.items))
    localStorage.setItem(this.key + ":ts", Date.now())
  }

  add(kvp) {
    this.items = { ...this.items, ...kvp } // merge
    this.save()
  }

  get(k) {
    return this.items[k]
  }

  del(k) {
    if (k in this.items) {
      delete this.items[k]
    }
    this.save()
  }

  has(k) {
    return k in this.items
  }

  load() {
    let stored = localStorage.getItem(this.key)
    console.log(`Loading cache for ${this.key}`)
    console.log(stored)
    return !stored ? [] : JSON.parse(stored) //this.isExpired() || !stored ? [] : JSON.parse(stored)
  }

  isExpired() {
    // if this cache never expires
    if (!this.expire || this.expire === -1) return false
    // but if it does expire...
    let whenCached = localStorage.getItem(this.key + ":ts")
    let age = (Date.now() - whenCached) / 1000
    console.log(`[Cache] is expired ${age} > `, age > this.expire)
    if (age > this.expire) {
      this.clear()
      return true
    } else {
      return false
    }
  }

  clear() {
    localStorage.removeItem(this.key)
    localStorage.removeItem(this.key + ":ts")
  }
}

/**
 * Caches objects as singletons. Alternative, can ignore this.items and just load
 * always from localStorage.
 * @type {Object}
 */

let cache = {}
/**
 * [getSingleton description]
 * @method getSingleton
 * @param  {string}     key           Key for localStorage
 * @param  {Number}     [expire=1200] Expiration time in seconds
 * @param  {String}     [sep=',']     separator in case. Default: ,
 * @return {CacheCollection}          A singleton of CacheCollection
 */
function getCache(key, expire = -1) {
  if (!cache.hasOwnProperty(key)) {
    // console.log('[getSingleton] Not in cache');
    cache[key] = new Cache(key, expire)
  }
  // console.log('[getSingleton]', cache[key]);
  return cache[key]
}

export { getCache, Cache }
