"use strict"

class Playa {
  constructor(videoel) {
    this.videodom = videoel

    this.source = document.createElement("source")

    this.source.setAttribute("src", "#")
    this.source.setAttribute("type", "video/mp4")

    this.videodom.appendChild(this.source)
    this.videodom.play()

    this.videodom.addEventListener(
      "ended",
      (evt) => {
        this.isPlaying = false
      },
      false
    )

    // this.source = null
    this.isPlaying = false
  }

  showOverlay(withtext) {
    let parent = document.getElementById("playaContainer")
    let ovr = document.getElementById("playaOverlay")
    var newone = ovr.cloneNode(true)
    newone.textContent = "" // clear all children

    // make child with new text
    let ovtxt = document.createElement("span")
    ovtxt.textContent = withtext
    newone.appendChild(ovtxt)

    parent.replaceChild(newone, ovr)
  }

  hideOverlay() {
    let ovr = document.getElementById("playaOverlay")
    ovr.hidden = true
  }

  setCallbackEnd(fn) {
    console.log(`Video ended in player`)
    this.videodom.addEventListener("ended", fn, false)
  }

  clearCallbackEnd() {
    this.videodom.removeEventListener("ended")
  }

  pause() {
    console.log(`⏸ pause`)
    this.videodom.pause()
    this.isPlaying = false
  }

  stop() {
    this.videodom.stop()
    this.isPlaying = false
  }

  resume() {
    this.videodom.play()
    this.isPlaying = true
  }

  togglePlayback() {
    if (!this.isPlaying) {
      this.play()
    } else {
      this.pause()
    }
  }

  mute() {
    this.videodom.mute = true
    console.log(`🔇 muting channel`)
  }

  unmute() {
    this.videodom.mute = false
    console.log(`🔈 unmuting channel`)
  }

  toggleMute() {
    if (this.videodom.mute) {
      this.unmute()
    } else {
      this.mute()
    }
  }

  loop(flag) {
    console.log(`⟳ loop set to ${flag}`)
    this.videodom.loop = flag
  }

  getStatus() {
    return { isplaying: this.isPlaying, url: this.getVideoUrl() }
  }

  getVideoUrl() {
    return this.source.getAttribute("src")
  }

  play(url, type) {
    console.log(`▶ play ${url}`)
    this.pause()

    //    this.videodom.removeAttribute("src") // empty source

    //    this.source = document.createElement("source")

    this.source.setAttribute("src", url)
    this.source.setAttribute("type", type)

    //  this.videodom.appendChild(this.source)
    this.videodom.load()
    this.videodom.play().catch((e) => {
      console.log(`${e}`)
    })
    this.loop(false)

    this.isPlaying = true

    // console.log({
    //   src: source.getAttribute("src"),
    //   type: source.getAttribute("type")
    // })
  }
}

export { Playa }

/*
var video = document.getElementById("playa")
var source = document.createElement("source")

source.setAttribute("src", current.url)
source.setAttribute("type", current.type)

video.appendChild(source)
video.play()

console.log({
  src: source.getAttribute("src"),
  type: source.getAttribute("type")
})

setInterval(() => {
  video.pause()

  currentIdx += 1
  if (currentIdx >= config.videos.length) {
    currentIdx = 0
  }

  current = config.videos[currentIdx]

  source.setAttribute("src", current.url)
  source.setAttribute("type", current.type)

  video.load()
  video.play()
}, 3000)
*/

/*
setTimeout(function () {
  video.pause()

  source.setAttribute(
    "src",
    "http://techslides.com/demos/sample-videos/small.webm"
  )
  source.setAttribute("type", "video/webm")

  video.load()
  video.play()
  console.log({
    src: source.getAttribute("src"),
    type: source.getAttribute("type")
  })
}, 3000)
*/
