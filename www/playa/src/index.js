import config from "./config.js"
import hotkeys from "hotkeys-js"
//import { Pane } from "tweakpane"
import { Playa } from "./playa.js"
import { getCache } from "./cache.js"
import * as mqtt from "mqtt"
import { ListenForAnyInteraction } from "./anyinteraction.js"

var domVideo = document.getElementById("playa")
var cache = getCache("thisplaya")
var playa = new Playa(domVideo)
var mqttclient = null
var mayTimeout = true

var anyinteraction = new ListenForAnyInteraction()

// console.log("anyinteraction -> ")
// console.log(anyinteraction)

anyinteraction.onInteraction(() => {
  if(mayTimeout) {
    console.log(`user interaction happened, will not timeout from config screen`);
    mayTimeout = false
  }
});

// handle what to do after a video ends playing back
// ////////////////////////////////////////////////////////////////////
playa.setCallbackEnd(() => {
  let id = cache.get("id")
  console.log(`Video ended on ${id}`)
  if (mqttclient) {
    mqttclient.publish(`events/ended`, `${id}`)
    console.log(
      `Sending message back to orchestrator that video ended in player with id '${id}'`
    )
  }
}) // setCallbackEnd

// UI event handlers
// ////////////////////////////////////////////////////////////////////////
document.getElementById("btnClear").addEventListener("click", () => {
  document.getElementById("player_id").value = null
  document.getElementById("mqttconn").value = null
  cache.clear()
})

document.getElementById("btnConfigOk").addEventListener("click", () => {
  let playaId = document.getElementById("player_id").value
  cache.add({ id: playaId })
  let mqttStr = document.getElementById("mqttconn").value
  cache.add({ mqtt: mqttStr })
  console.info(`Playa is configured with ID ${playaId}`)
  console.info(`Pointing to mqtt broker ${mqttStr}`)
  document.getElementById("playaIntro").hidden = true
  mqtt_connect(playaId, mqttStr)
})


// ////////////////////////////////////////////////////////////////////////
if (cache.has("id") && cache.has("mqtt")) {
  console.log(`Found iD for player = ${cache.items["id"]}`)
  console.log(`Found mqtt connection string = ${cache.items["mqtt"]}`)
  config.id = cache.get("id")
  config.mqtt_broker = cache.get("mqtt")
  document.getElementById("player_id").value = cache.get("id")
  document.getElementById("mqttconn").value = cache.get("mqtt")
  document.getElementById("btnConfigOk").focus()
  if (config.skip_config) {
    setTimeout(() => {
      // if there's been no user interaction for some time, 
      // simulate a click on the OK button to proceed
      if(mayTimeout) {
        document.getElementById("btnConfigOk").click()
      }
    }, 5000)
  }
} else {
  document.getElementById("player_id").focus()
  console.log("Player has no iD associated yet...")
}

// //////////////////////////////////////////////////////////////
// MQTT pub/sub
// //////////////////////////////////////////////////////////////
function mqtt_connect(id, connstr) {
  console.info(`Connnecting to mqtt broker ${connstr}`)
  try {
    let url = new URL(connstr)
    let iswss = url.protocol === "wss:" ? true : false
    if (!iswss) {
      console.error(
        `${connstr} is not a websockets address, choose a wss:// address`
      )
    }
  } catch (err) {
    console.log(`Failed to parse valid URL from ${connstr}`)
  }

  mqttclient = mqtt.connect(connstr, { clientId: id })

  mqttclient.on("connect", function () {
    mqttclient.subscribe("presence", function (err) {
      if (!err) {
        mqttclient.publish("presence", id)
      }
    })
  })

  mqttclient.subscribe(`${id}/#`, function (err) {
    if (err) {
      console.error(`something went wrong when subscribing to ${id}/url: `, err)
    } else {
      console.info(`))o(( subscribed to channel ${id}/url`)
    }
  })

  mqttclient.on("message", function (topic, message) {
    //console.log({ topic: topic, message: message.toString() })
    if (topic.includes("url")) {
      playa.play(message.toString(), "video/mp4")
    } else if (topic.includes("overlay")) {
      playa.showOverlay(message.toString())
    } else if (topic.includes("command")) {
      let whatcmd = message.toString()
      console.log(`player received command <- '${whatcmd}'`)
      switch (whatcmd) {
        case "mute":
          playa.mute()
          break
        case "unmute":
          playa.unmute()
          break
        case "resume":
          playa.play()
          break
        case "pause":
          playa.pause()
          break
        case "loop":
          playa.loop(true)
          break
        case "noloop":
          playa.loop(false)
          break
        default:
          console.log(`Unknown command received '${whatcmd}'`)
          break
      } // switch
    } else if (topic.includes("status")) {
      let id = cache.get("id")
      let stat = playa.getStatus()
      let payload = {
        playerid: id,
        ...stat
      }
      console.log("⇊⇊ sending back player status ⇊⇊")
      console.log(payload)
      mqttclient.publish(`status`, JSON.stringify(payload))
    }
  })
} // mqtt_connect

/*
if (cache.get("id")) {
  let connstr = cache.get("mqtt");
  mqtt_connect(cache.get("id"), connstr);
} // if a player id was defined
*/

/*
client.on('message', function (topic, payload, packet) {
  // Payload is Buffer
  console.log(`Topic: ${topic}, Message: ${payload.toString()}, QoS: ${packet.qos}`)
})
*/

var currentIdx = 0
var current = config.videos[currentIdx]

// parameter settings gui
// const pane = new Pane()
// pane.addInput(config, "id")
// pane.addButton({ title: "Save settings" })

// keyboard handler
hotkeys("space", function (event, handler) {
  playa.togglePlayback()
})

hotkeys("m", function (event, handler) {
  playa.toggleMute()
})

/*
var video = document.getElementById("playa")
var source = document.createElement("source")

source.setAttribute("src", current.url)
source.setAttribute("type", current.type)

video.appendChild(source)
video.play()

console.log({
  src: source.getAttribute("src"),
  type: source.getAttribute("type")
})

setInterval(() => {
  video.pause()

  currentIdx += 1
  if (currentIdx >= config.videos.length) {
    currentIdx = 0
  }

  current = config.videos[currentIdx]

  source.setAttribute("src", current.url)
  source.setAttribute("type", current.type)

  video.load()
  video.play()
}, 3000)
*/
