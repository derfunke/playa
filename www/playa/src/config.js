module.exports = {
  skip_config: true,
  mqtt_broker: "wss://interact:SkRYnMNLx5XoI4yC@interact.cloud.shiftr.io",
  videos: [
    {
      url: "https://cdn.derfunke.net/videos/abstract.mp4",
      type: "video/mp4",
      title: "Abstraction"
    },
    {
      url: "https://cdn.derfunke.net/videos/tunnel.mp4",
      type: "video/mp4",
      title: "Tunnel"
    },
    {
      url: "https://cdn.derfunke.net/videos/dancer.mp4",
      type: "video/mp4",
      title: "Dancer"
    },
    {
      url: "https://cdn.derfunke.net/videos/train.mp4",
      type: "video/mp4",
      title: "Train"
    },
    {
      url: "https://cdn.derfunke.net/videos/dancer2.mp4",
      type: "video/mp4",
      title: "Dancer palm"
    },
    {
      url: "https://cdn.derfunke.net/videos/wireframe.mp4",
      type: "video/mp4",
      title: "Wireframe"
    }
  ],
  id: "id12345"
}
