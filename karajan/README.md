## Karajan the conductor

Karajan is the orchestration server built to fit playa, it has all the elements that you need to run a Playa session. Karajan acts as a conductor for your choir of devices.

Karajan is designed to run as a self-contained server on a raspberry pi and it provides two services that are fundamental for Playa, a file server to host video files and an mqtt broker to coordinate all choir's devices.

To create your Karajan node, type: `sudo raspbian/installkarajan.sh`, this will install all the necessary dependencies.




