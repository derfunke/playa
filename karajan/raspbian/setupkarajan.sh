#!/bin/bash

if [ "$EUID" -ne 0 ]
	then echo "Please become root, you need to be root to run this. Thanks!"
	exit
fi

## ANSI color table
Black="\033[0;30m"        # Black
Red="\033[0;31m"          # Red
Green="\033[0;32m"        # Green
Yellow="\033[0;33m"       # Yellow
Blue="\033[0;34m"         # Blue
Purple="\033[0;35m"       # Purple
Cyan="\033[0;36m"         # Cyan
White="\033[0;37m"        # White
NC="\033[0m"              # No Color

upgrade() {
  echo -e "${Yellow} Upgrading distro... ${NC}"
  apt update && apt upgrade -y
}

filebrowser() {
  echo -e "${Yellow} Installing filebrowser distro... ${NC}"
  # install filebrowser
  curl -fsSL https://raw.githubusercontent.com/filebrowser/get/master/get.sh | bash
# filebrowser -r ./Videos

# starting filebrowser app
# will launch on port 8080
# filebrowser -a 0.0.0.0 -r /path/to/your/files
}

fixlocale() {
  echo -e "${Yellow} Fixing locale issue... ${NC}"
  # fix locale issue
  export LANGUAGE=en_US.UTF-8
  export LANG=en_US.UTF-8
  export LC_ALL=en_US.UTF-8
  locale-gen en_US.UTF-8
  dpkg-reconfigure locales
}

mosquitto() {
  echo -e "${Yellow} Installing mosquitto... ${NC}"
  # install mosquitto
  apt install -y mosquitto mosquitto-clients

  systemctl enable mosquitto.service

  mosquitto -v
}


docker_install() {
  echo -e "${Yellow} Installing docker and docker-compose... ${NC}"
  ## docker install
  curl -sSL https://get.docker.com | sh
  apt-get install -y libffi-dev libssl-dev
  usermod -aG docker pi
  apt install -y python3-dev
  apt-get install -y python3 python3-pip
  pip3 install docker-compose
  echo -e "${Green} Enabling docker as a system service... ${NC}"
  systemctl enable docker
}

docker_test() {
  echo "${Green} Trying out docker install... ${NC}"
  docker run hello-world
}


if [[ $(which docker) && $(docker --version) ]]; then
    echo "Docker Ok."
  else
    docker_install
fi

docker_test

